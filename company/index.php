<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><p>
	 Аспро: Максимум — интернет-магазин товаров для активного отдыха и туризма. Здесь вы найдете все от удочек и туристических палаток до сноубордов и пневматических пистолетов. Мы работаем с 2005 года и сотрудничаем с проверенными и известными производителями. Их товары соответствуют всем требованиям и ГОСТам.
</p>
<h3>Клиентский сервис на высоте!</h3>
<p>
	 Наши менеджеры помогут выбрать подходящий вид лески, сноуборда или спального мешка. Подскажут, как оформить заказ и совершить оплату. Менеджеры отвечают на вопросы в онлайн-чате и через формы обратной связи. Среднее время ответа — 10 минут. Также вы можете позвонить по номеру +7 (000) 000-00-00.
</p>
<blockquote>
	 Учитываем историю покупок и общения, чтобы персонализировать ответы и предложения. Нам важно решить вопрос быстро и качественно. Формирование долгосрочных отношений — одна из главных целей.
</blockquote>
<h3>
Комплексный подход к товарам и услугам </h3>
<p>
	 Покупка товара проста только на первый взгляд. Нужно думать о том, как найти нужный товар, оформить покупку и доставку. В нашем магазине вам не придется тратить на это время. Удобный сайт не даст вам заблудиться среди страниц, а наша команда поможет со всеми вопросами! Мы предлагаем:
</p>
<ul>
	<li>Доставку товаров до квартиры в течение 24 часов.</li>
	<li>Менеджера, который знает детали заказа.</li>
	<li>Услуги по аренде, ремонту и обслуживанию.</li>
</ul>
<p>
	 Сделаем жизнь ярче и комфортнее. В нашем интернет-магазине найдется товар, который нужен именно вам. Если возникнут вопросы, пишите в онлайн-чат или звоните по номеру +7 (000) 000-00-00. С удовольствием поможем с выбором и подскажем детали заказа.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>